public class SearchData {
    public static void getSearchResults(String searchTerm){
        List<List<sObject>> accList = [FIND :searchTerm IN ALL FIELDS 
                                        RETURNING Account(Name), Contact(LastName)];
        System.debug(accList);
    }
}