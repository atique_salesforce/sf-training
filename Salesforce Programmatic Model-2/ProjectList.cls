public class ProjectList {
    public ProjectList() {}
    public static void getProject(Id userId) {
        List<Project__c> projectList = [Select Id, Name, Status__c
                                       From Project__c
                                       Where OwnerId = :userId];
        System.debug(projectList);
    }
    public static void addProject(Project__c project) {
        insert project;
        System.debug(project);
    }
    public static void deleteProject(Id projectDelId) {
        List<Project__c> projectDeleteList = [Select Id, Name
                                             From Project__c
                                             Where Id = :projectDelId];
        delete projectDeleteList;
    }
    public static void editProject() {}
}