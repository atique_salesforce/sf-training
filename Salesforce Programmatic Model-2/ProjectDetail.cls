public class ProjectDetail {
    public ProjectDetail() {}
    public static void getProjectDetails(Id projectId) {
        List<Project__c> projectList = [Select Id, Name  
                                        FROM Project__c 
                                        Where Id = :projectId];
        System.debug(projectList);
    }
    public static void getTasks(Id projectId) {
        List<Task__c> taskList = [Select Id, Name  
                               	From Task__c 
                               Where Project1__c = :projectId];
        System.debug(taskList);
    }
    public static void editProject() {}
}