/**
 * @author: Atique Chaudhary
 * @description: Test Class for TestBillingHelper Class
 * @date:  18-03-2021
 **/

@isTest
public with sharing class TotalTriggerTest {

    /**
     * @author: Atique Chaudhary
     * @date:  18-03-2021
     * Case 1 for triggerHandler()
     * @description: Method to check if Null values can be passed
     * @input: null
     * @output: None
     **/

    @isTest public static void beforeTaskInsert(){
        List<Task__c> taskList = new List<Task__c>();
        TaskTriggerHelper.triggerHandler(taskList);
    }
    /**
     * @author: Atique Chaudhary
     * @date:  18-03-2021
     * Case 2 for triggerHandler()
     * @description: Method to give exception for Required Fields
     * @input: taskList with blank values
     * @output: Values Remain Null
     **/


    @isTest
    static void requiredFieldsCheck()
    {
        Task__c task = new Task__c();
        try
        {
            insert task;
        }
        catch(DmlException e)
        {
            for(Integer index; index < e.getNumDml(); index++)
            {
            System.assertEquals('Required fields are missing: [Name,Estimated_Hours__c, Billable_Hours__c, Non_Billable_Hours__c, Project__c]: [Name,Estimated_Hours__c, Billable_Hours__c, Non_Billable_Hours__c, Project__c]',e.getDmlMessage(index));
            }
        }
    }
    /**
     * @author: Atique Chaudhary
     * @date:  18-03-2021
     * Case 3 for triggerHandler()
     * @description: Method to check if tasklist passes Id's in Set
     * @input: Number of Inputs in taskList
     * @output: Multiple Task records
     **/

    @isTest
    public static void beforeTaskBulkCheck(){
        TriggerTestDataFactory.createBulkTask(100);
    }

    /**
     * @author: Atique Chaudhary
     * @date:  18-03-2021
     * Case 4 for triggerHandler()
     * @description: Method to check if Billable, Non-Billable Hours are Equal to Total Billable and Non-Billable Hours
     * @input: Number of Inputs in taskList
     * @output: Multiple Task records
     **/
    
    @isTest public static void beforeTaskInsertBulkify(){

        Project__c project = TriggerTestDataFactory.createBulkTask(100);

        //Calling data factory and specifying number of new task to create
        TriggerTestDataFactory.createBulkTask(100);
        List<Project__c> projectList = [SELECT Billable_Hours__c,Non_Billable_Hours__c
                                         FROM Project__c 
                                        WHERE Id = :project.Id];
                                        
        Decimal totalBillable = projectList[0].Billable_Hours__c;
        Decimal totalNonBillable = projectList[0].Non_Billable_Hours__c ;

        System.assertEquals(4000, totalBillable);
        System.assertEquals(5000, totalNonBillable);
    }
}