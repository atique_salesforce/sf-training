public class TaskTriggerHelper {
    public static void  TriggerHandler(List<Task__c> taskList){
        if(taskList == null){
            return;
        }
        Set<Id> projectIdSet = new Set<Id>();

        for(Task__c counter: taskList){
            projectIdSet.add(counter.Project__c);
        }
       
        List<Project__c> projectList = [SELECT Id,Billable_Hours_Trigger__c,Non_Billable_Hours_Trigger__c
                                       FROM Project__c
                                       WHERE Id IN :projectIdSet];
       
        List<AggregateResult> taskSumList = [SELECT Project__c,Sum(Billable_Hours__c)billable, Sum(Non_Billable_Hours__c)nonBillable
                                            FROM Task__c
                                            WHERE Project__c IN :projectIdSet
                                            GROUP BY Project__c];
       
        for(Project__c projectCounter: projectList){
            for(AggregateResult agr: taskSumList){
                projectCounter.Billable_Hours_Trigger__c = (decimal)agr.get('billable');
                projectCounter.Non_Billable_Hours_Trigger__c = (decimal)agr.get('nonBillable');
               }
        }
        update projectList;
    }
}