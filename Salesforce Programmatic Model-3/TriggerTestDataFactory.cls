/**
 * @author: Atique Chaudhary
 * @description: Test Data Factory Class 
 * @date:  18-03-2021
 **/
@isTest
public class TriggerTestDataFactory{
    /**
     * @author: Atique Chaudhary
     * @date:  18-03-2021
     * @description: Data factory helper method to create new Project and its related tasks
     * @input: Number of task to be created
     * @output: Project record
     **/
    public static Project__c createBulkTask(Integer count){
        
        Project__c project = new Project__c(Name = 'Atique');
        insert project;

        List<Task__c> taskList = new List<Task__c>();
        for(Integer index = 0; index < count ; index++){
            taskList.add(new Task__c(Name = 'Project' + index, Billable_Hours__c = 40, Non_Billable_Hours__c = 50,
                                      Project1__c = project.Id, Project__c = project.Id));
        }
        insert taskList;
        return project;
    }
}