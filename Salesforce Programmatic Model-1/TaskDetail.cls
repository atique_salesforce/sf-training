/**
 * @author: Atique Chaudhary
 * @description: Class to Get Task Details
 * @date:  12-03-2021
 **/
 
public class TaskDetail {

    /**
    * @author: Atique Chaudhary
    * @date: 12-03-2021
    * @description: Method to get Task Details
    * @input: Null
    * @output: Null
    **/
    public static void getTaskDetail() {}
    
    /**
    * @author: Atique Chaudhary
    * @date: 12-03-2021
    * @description: Method to edit Task Details
    * @input: Null
    * @output: Null
    **/
    public static void editTask() {}
}