/**
 * @author: Atique Chaudhary
 * @description: Test Class for TestBillingHelper Class
 * @date:  12-03-2021
 **/

public with sharing class TaskDetailController {
       
       /**
     * @author: Atique Chaudhary
     * @date:  12-03-2021
     * @description: Method to create instance and add values into set and map.
     * @input: null
     * @output: Returns Map,Set and List.
     **/
     
      public static void getTaskDetailController() {
 
        List<Task> listTask = new list<Task>();
        Task taskObject = new Task();
        Task taskObject1 = new Task();
        Task taskObject2 = new Task();
 
        taskObject.Description = 'Development';
        taskObject.WhoId = '0032w00000LR5yAAAT';
        taskObject.Subject = 'Development';
 
        taskObject1.Description = 'Review';
        taskObject1.WhoId = '0032w00000LR5yAAAT';
        taskObject1.Subject = 'Review';
        
        taskObject2.Description = 'Requirement Gathering';
        taskObject2.WhoId = '0032w00000LR5yAAAT';
        taskObject2.Subject = 'Requirement Gathering';
       
        listTask.add(taskObject);
        listTask.add(taskObject1);
        listTask.add(taskObject2);
 
        insert listTask;
 
        Set<String> taskNameSet = new Set<String>();
        for(Task tasktemp: listTask)
        {
            taskNameSet.add(tasktemp.WhoId);
        }
        System.debug(taskNameSet);
 
        Map<String,String> taskNameMap = new Map<String,String>();
        for(Task tasktemp2: listTask)
        {
            taskNameMap.put(tasktemp2.Subject, tasktemp2.WhoId);
        }
        System.debug(taskNameMap);
        
        return taskNameMap;
        return taskNameSet;
        return listTask;
    }
}