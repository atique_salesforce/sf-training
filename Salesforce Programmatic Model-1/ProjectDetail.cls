/**
* @author: Atique Chaudhary
* @description: Class to Get Project Details
* @date: 12-03-2021
**/
public class ProjectDetail {

    /**
    * @author: Atique Chaudhary
    * @date: 12-03-2021
    * @description: Method to get project details
    * @input: Null
    * @output: Null
    **/
    public static void getProjectDetails() {}
    
    /**
    * @author: Atique Chaudhary
    * @date: 12-03-2021
    * @description: Method to get task details
    * @input: Null
    * @output: Null
    **/
    public static void getTasks() {}
    
    /**
    * @author: Atique Chaudhary
    * @date: 12-03-2021
    * @description: Method to edit project details
    * @input: Null
    * @output: Null
    **/
    public static void editProject() {}
}