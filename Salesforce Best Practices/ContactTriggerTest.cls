@isTest
public class ContactTriggerTest {
    @isTest
    public static void contactTriggerTestHandler(){
        
        Account account = new Account(Name = 'Test account', Last_Name__c = 'Account', 
                                        First_Name__c = 'Test', Out_Of_Business__c = TRUE);
        insert account;
       
  		List<Contact> contactList = new List<Contact>();
        for(Integer index = 0; index <3 ; index++){
            contactList.add(new Contact(LastName = 'New Test ' + index, AccountId = account.Id));
        }
                       
        contactList[0].Is_Primary__c = TRUE;
        insert contactList;
       
        Account accountDetails = [SELECT Id, Primary_Contact__c
                        	      FROM Account
                        	      WHERE Id =: account.Id];
       
        System.assertEquals(accountDetails.Primary_Contact__c, contactList[0].Id);
       
    }

}