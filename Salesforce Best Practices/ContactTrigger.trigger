trigger ContactTrigger on Contact (after insert, after update) {
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate){
            ContactTriggerHandler.ContactTriggerHandlerMethod(Trigger.New);
        }
    }       
}