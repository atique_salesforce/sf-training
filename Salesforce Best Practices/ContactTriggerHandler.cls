public class ContactTriggerHandler {
    public static void ContactTriggerHandlerMethod(List<Contact> contactList){
        Set<Id> accountIdSet = new Set<Id>();
        List<Contact> updatedContactList = contactList;
        for (Contact contObj : contactList) {
        accountIdSet.add(contObj.AccountId);
        }
     
        List<Account> accountList = [SELECT Id, Primary_Contact__c,
                                      (SELECT Id, Name, Is_Primary__c
                                       FROM Contacts )
                                      FROM Account
                                      WHERE Id IN :accountIdSet];

        for(Account accObj: accountList){
            for(Contact conObj: accObj.Contacts){
                if(conObj.Is_Primary__c == TRUE){
                    accObj.Primary_Contact__c = conObj.Id;
                }
                else {
                    accObj.Primary_Contact__c = null;
                }
            }
        }
        update accountList;   
    }
}